# k8s-test

Quick repo to learn how k8s works.

This will be using the `kube-system` namespace.

## Requirements

 - [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
 - [helm](https://helm.sh/)
 - kubernetes ([minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/))
 
## Installation

1. Begin the process by initialising helm into your cluster
    
    ```
    helm init
    ```

2. Install [Traefik](https://traefik.io/) using helm.

    ```
    helm install stable/traefik --name traefik --namespace kube-system --values helm/traefik/values.yaml
    ```
    
3. Create a service and ingress for the traefik dashboard.

    ```
    kubectl apply -f helm/traefik/ingress.yaml
    ```

4. Add an entry to the hosts file to enable you to access the dashboard

    ```
    echo "$(minikube ip) traefik-ui.minikube" | sudo tee -a /etc/hosts
    ```

5. Visit the dashboard for traefik to confirm it is working [http://traefik-ui.minikube](http://traefik-ui.minikube)

6. Deploy the example app to the cluster using helm

    ```
    helm install --name test --namespace kube-system ./helm/k8s-test
    ```
    
7. Check the [Traefik dashboard](http://traefik-ui.minikube) to see the new service running.

8. Add another entry to the hosts file
    ```
    echo "$(minikube ip) k8s-test.local" | sudo tee -a /etc/hosts
    ```

9. Go to [http://k8s-test.local](http://k8s-test.local) and see the app running.
